import * as React from 'react';
import { ChangeEvent, useState } from 'react';
import Button from '../components/Button';
import Card, { CardProps } from '../components/Card';
import './style.css'

function Main() {
  const [studentName, setStudentName] = useState<string>('')
  const [students, setStudents] = useState<CardProps[]>([]);

  function handleStudents() {
    const newStudent = {
      name: studentName,
      time: new Date().toLocaleTimeString('pt-br', { hour: '2-digit', minute: '2-digit' }),
      date: new Date().toLocaleDateString('pt-br')
    }
    setStudents((prevState: CardProps[]) => [...prevState, newStudent])
  }

  return (
    <div className="container">

        <input
          placeholder='Digite seu nome'
          onChange={(e: any) => setStudentName(e.target.value)}
        />
        <Button
          handleStudent={handleStudents} />
     
      {students.map((student, index) =>
        <Card
          key={index}
          name={student.name}
          time={student.time}
          date={student.date}
        />)}

    </div>
  )
}

export default Main
