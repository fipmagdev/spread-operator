import * as React from 'react';
import './style.css'

export interface CardProps {
    name: string,
    time: string,
    date: string
}

function Card({ name, time, date }: CardProps) {
    return (
        <div className='card-container'>
            <strong>{name}</strong>
            <small>{time}</small>
            <small>{date}</small>
        </div>
    );
}

export default Card;