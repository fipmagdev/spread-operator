import * as React from 'react';
import './style.css'
interface buttonProps {
    handleStudent: () => void
}
function Button({ handleStudent }: buttonProps) {
    return (
        <button
            type='submit'
            className='button-submit'
            onClick={handleStudent}
        >
            <p>
                Estou presente
            </p>
        </button>
    );
}

export default Button;